import { LoopSettings } from "./types/LoopSettings";
import { GameLoop } from "./types/GameLoop";
export declare function createLoop(settings: LoopSettings): GameLoop;
