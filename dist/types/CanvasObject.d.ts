export type CanvasObject = {
    canvas: HTMLCanvasElement;
    context: CanvasRenderingContext2D;
};
