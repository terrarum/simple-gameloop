export type CanvasOptions = {
    containerSelector?: string;
    classes?: string;
    id?: string;
    width?: number;
    height?: number;
};
