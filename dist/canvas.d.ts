import { CanvasOptions } from "./types/CanvasOptions";
import { CanvasObject } from "./types/CanvasObject";
export declare function createCanvas(canvasOptions: CanvasOptions): CanvasObject;
