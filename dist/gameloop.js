export function createLoop(settings) {
    let isPlaying = true;
    let rafId = 0;
    let speed = 1;
    function frameFactory(settings) {
        let last = 0;
        const step = (timeElapsedMs) => {
            if (last === 0) {
                last = timeElapsedMs;
            }
            const deltaTimeMs = timeElapsedMs - last;
            const deltaTimeS = deltaTimeMs / 1000;
            if (isPlaying) {
                settings.update(deltaTimeS * speed);
            }
            settings.render();
            last = timeElapsedMs;
            rafId = window.requestAnimationFrame(step);
        };
        return step;
    }
    rafId = window.requestAnimationFrame(frameFactory(settings));
    return {
        play: () => isPlaying = true,
        pause: () => isPlaying = false,
        setSpeed: (newSpeed) => speed = newSpeed,
        destroy: () => window.cancelAnimationFrame(rafId)
    };
}
