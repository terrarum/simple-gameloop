// Logic taken from https://github.com/mrdoob/stats.js/
export default class FPSMeter {
    frames = 0;
    beginTime = performance.now();
    prevTime = this.beginTime;
    fps = 0;
    begin() {
        this.beginTime = performance.now();
    }
    end() {
        this.frames++;
        const time = performance.now();
        if (time >= this.prevTime + 1000) {
            this.fps = ((this.frames * 1000) / (time - this.prevTime));
            this.prevTime = time;
            this.frames = 0;
        }
        return this.fps;
    }
}
