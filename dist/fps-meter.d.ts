export default class FPSMeter {
    private frames;
    private beginTime;
    private prevTime;
    private fps;
    begin(): void;
    end(): number;
}
