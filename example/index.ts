import { createLoop, createCanvas, type CanvasObject, type GameLoop } from '../dist/index';

let canvasObject: CanvasObject;
let gameLoop: GameLoop;

const canvasWidth: number = 400;
const canvasHeight: number = 200;

const box = {
    x: 100,
    y: 40,
    width: 30,
    height: 20,
};

function create() {
    gameLoop?.destroy();
    canvasObject?.canvas.remove();

    canvasObject = createCanvas({
        width: canvasWidth,
        height: canvasHeight,
        containerSelector: '#canvas-container',
    });

    gameLoop = createLoop({
        update,
        render,
    });

    box.x = 100;
}

/**
 * Update the state of the scene.
 *
 * @param dt - time in seconds since last call to update function
 */
const update = function update(dt: number) {
    // Move square right at 50 pixels per second.
    box.x += 50 * dt;

    // Wrap the square back to the left edge if it goes off the right edge.
    box.x = box.x > canvasWidth ? -box.width : box.x;

    console.log('updating');
};

/**
 * Draw the scene.
 */
const render = function render() {
    if (!canvasObject.context) {
        return;
    }

    // Reset the canvas.
    canvasObject.context.clearRect(0, 0, canvasObject.context.canvas.width, canvasObject.context.canvas.height);

    // Draw square.
    canvasObject.context.fillRect(box.x, box.y, box.width, box.height);
};

function init() {
    create();

    document.getElementById('play')?.addEventListener('click', () => gameLoop.play());
    document.getElementById('pause')?.addEventListener('click', () => gameLoop.pause());
    document.getElementById('destroy')?.addEventListener('click', () => {
        gameLoop.destroy();
        canvasObject.canvas.remove();
    });
    document.getElementById('create')?.addEventListener('click', () => create());
}

init();
