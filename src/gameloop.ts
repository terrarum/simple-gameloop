import {LoopSettings} from "./types/LoopSettings";
import {GameLoop} from "./types/GameLoop";

export function createLoop(settings: LoopSettings): GameLoop {
    let isPlaying: boolean = true;
    let rafId: number = 0;
    let speed: number = 1;

    function frameFactory(settings: LoopSettings): FrameRequestCallback {
        let last: number = 0;

        const step = (timeElapsedMs: number) => {
            if (last === 0) {
                last = timeElapsedMs;
            }

            const deltaTimeMs: number = timeElapsedMs - last;
            const deltaTimeS: number = deltaTimeMs / 1000;

            if (isPlaying) {
                settings.update(deltaTimeS * speed);
            }

            settings.render();

            last = timeElapsedMs;
            rafId = window.requestAnimationFrame(step);
        }

        return step;
    }

    rafId = window.requestAnimationFrame(frameFactory(settings));

    return {
        play: (): boolean => isPlaying = true,
        pause: (): boolean => isPlaying = false,
        setSpeed: (newSpeed: number): number => speed = newSpeed,
        destroy: (): void => window.cancelAnimationFrame(rafId)
    }
}
