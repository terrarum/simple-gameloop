// Logic taken from https://github.com/mrdoob/stats.js/
export default class FPSMeter {
  private frames: number = 0;
  private beginTime: number = performance.now();
  private prevTime: number = this.beginTime;
  private fps: number = 0;

  public begin(): void {
    this.beginTime = performance.now();
  }

  public end(): number {
    this.frames++;
    const time: number = performance.now();
    if (time >= this.prevTime + 1000) {
      this.fps = ((this.frames * 1000) / (time - this.prevTime));
      this.prevTime = time;
      this.frames = 0;
    }
    return this.fps;
  }
}
