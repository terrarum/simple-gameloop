import {CanvasOptions} from "./types/CanvasOptions";
import {CanvasObject} from "./types/CanvasObject";

export function createCanvas(canvasOptions: CanvasOptions): CanvasObject {
    if (canvasOptions.containerSelector == null) {
        canvasOptions.containerSelector = 'body';
    }

    const containerEl = document.querySelector(canvasOptions.containerSelector);

    if (containerEl == null) {
        throw new Error(`Could not create element for selector ${canvasOptions.containerSelector}`);
    }

    const canvas = document.createElement('canvas');

    if (canvasOptions.classes != null) {
        const classNames = canvasOptions.classes.split(' ');
        classNames.forEach(className => canvas.classList.add(className));
    }

    const context = canvas.getContext('2d');

    if (context == null) {
        throw new Error('Could not get context from canvas');
    }

    if (canvasOptions.width != null) {
        canvas.width = canvasOptions.width;
    }
    if (canvasOptions.height != null) {
        canvas.height = canvasOptions.height;
    }

    containerEl.appendChild(canvas);

    return {
        canvas,
        context
    }
}
