export type LoopSettings = {
  update: (dt: number) => void;
  render: () => void;
}
