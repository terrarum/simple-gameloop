export type GameLoop = {
  play: () => void;
  pause: () => void;
  setSpeed: (newSpeed: number) => void
  destroy: () => void;
}
