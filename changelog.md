# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.

## [3.0.0] - 2025-02-22
### Changed
- createLoop now returns playback controls
- improved the readme

### Removed
- references to the update being fixed

## [1.0.4] - 2024-06-18
### Changed
- Moved Parcel to devDependencies
- Updated README.md

## [1.0.3] - 2018-10-22
### Added
- Working demo in readme and `/example`

## [1.0.2] - 2018-10-21
### Added
- Bugs and Keywords fields to package.json
- This changelog

## [1.0.1] - 2018-10-21
### Added
- Repository field to package.json

## [1.0.0] - 2018-10-21
### Added
- createLoop
- createCanvas
